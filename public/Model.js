var Model = function() {

   var gl;

   var vao;
   var vbo;
   var numTriangles = 0;

   var position = {x: 0, y: 0, z: 0, pitch: 0, yaw: 0};
   var scale = {x: 1, y: 1, z: 1};
   var modelMatrix = new Matrix4();
   var textureID;
   var hasTexture = false;


   function InitGL(webgl) {
      gl = webgl;

      vao = gl.createVertexArray();
      vbo = gl.createBuffer();

      var verts = [
         // Front Face
         -0.5, -0.5, 0.5,     0, 0,       0, 0, 1,
         0.5, -0.5, 0.5,      0, 0,       0, 0, 1,
         0.5, 0.5, 0.5,       0, 0,       0, 0, 1,

         0.5, 0.5, 0.5,       0, 0,       0, 0, 1,
         -0.5, 0.5, 0.5,      0, 0,       0, 0, 1,
         -0.5, -0.5, 0.5,     0, 0,       0, 0, 1,

         // Right Face
         0.5, -0.5, 0.5,      0, 0,       1, 0, 0,
         0.5, -0.5, -0.5,     0, 0,       1, 0, 0,
         0.5, 0.5, -0.5,      0, 0,       1, 0, 0,

         0.5, 0.5, -0.5,      0, 0,       1, 0, 0,
         0.5, 0.5, 0.5,       0, 0,       1, 0, 0,
         0.5, -0.5, 0.5,      0, 0,       1, 0, 0,

         // Back Face
         0.5, -0.5, -0.5,     0, 0,       0, 0, -1,
         -0.5, -0.5, -0.5,    0, 0,       0, 0, -1,
         -0.5, 0.5, -0.5,     0, 0,       0, 0, -1,

         -0.5, 0.5, -0.5,     0, 0,       0, 0, -1,
         0.5, 0.5, -0.5,      0, 0,       0, 0, -1,
         0.5, -0.5, -0.5,     0, 0,       0, 0, -1,

         // Left Face
         -0.5, -0.5, -0.5,    0, 0,       -1, 0, 0,
         -0.5, -0.5, 0.5,     0, 0,       -1, 0, 0,
         -0.5, 0.5, 0.5,      0, 0,       -1, 0, 0,

         -0.5, 0.5, 0.5,      0, 0,       -1, 0, 0,
         -0.5, 0.5, -0.5,     0, 0,       -1, 0, 0,
         -0.5, -0.5, -0.5,    0, 0,       -1, 0, 0,

         // Top Face
         -0.5, 0.5, 0.5,      0, 0,       0, 1, 0,
         0.5, 0.5, 0.5,       0, 0,       0, 1, 0,
         0.5, 0.5, -0.5,      0, 0,       0, 1, 0,

         0.5, 0.5, -0.5,      0, 0,       0, 1, 0,
         -0.5, 0.5, -0.5,     0, 0,       0, 1, 0,
         -0.5, 0.5, 0.5,      0, 0,       0, 1, 0,

         // Bot Face
         -0.5, -0.5, -0.5,    0, 0,       0, -1, 0,
         0.5, -0.5, -0.5,     0, 0,       0, -1, 0,
         0.5, -0.5, 0.5,      0, 0,       0, -1, 0,

         0.5, -0.5, 0.5,      0, 0,       0, -1, 0,
         -0.5, -0.5, 0.5,     0, 0,       0, -1, 0,
         -0.5, -0.5, -0.5,    0, 0,       0, -1, 0
      ];

      SetVertexData(verts);
   }

   
   function Draw(shader) {
      gl.bindVertexArray(vao);

      if (hasTexture) {
         gl.activeTexture(gl.TEXTURE0);
         gl.bindTexture(gl.TEXTURE_2D, textureID);
         shader.Uniform1i("tex", 0);
      }

      shader.UniformMatrix4fv("model", modelMatrix);

      gl.drawArrays(gl.TRIANGLES, 0, numTriangles);

      gl.bindVertexArray(null);
   }


   function SetPosition(x, y, z) {
      position.x = x;
      position.y = y;
      position.z = z;

      modelMatrix = Matrix4.Multiply(
         Matrix4.Scale(scale.x, scale.y, scale.z), 
         Matrix4.Translate(x, y, z));
   }


   function SetScale(x, y, z) {
      scale.x = x;
      scale.y = y;
      scale.z = z;

      modelMatrix = Matrix4.Multiply(
         Matrix4.Scale(x, y, z), 
         Matrix4.Translate(position.x, position.y, position.z));
   }


   function SetTextureID(id) {
      textureID = id;
      hasTexture = true;
   }


   function SetVertexData(vertexData) {
      numTriangles = Math.floor(vertexData.length / 8);

      gl.bindVertexArray(vao);
      
      gl.bindBuffer(gl.ARRAY_BUFFER, vbo);
      gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertexData), gl.STATIC_DRAW);
      gl.vertexAttribPointer(0, 3, gl.FLOAT, false, 32, 0);
      gl.enableVertexAttribArray(0);
      gl.vertexAttribPointer(1, 2, gl.FLOAT, false, 32, 12);
      gl.enableVertexAttribArray(1);
      gl.vertexAttribPointer(2, 3, gl.FLOAT, false, 32, 20);
      gl.enableVertexAttribArray(2);

      gl.bindVertexArray(null);
   }


   return {
      InitGL: InitGL,
      Draw: Draw,
      SetPosition: SetPosition,
      SetScale: SetScale,
      SetTextureID: SetTextureID,
      SetVertexData: SetVertexData,
   }
};