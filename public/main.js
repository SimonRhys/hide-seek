(function () {

var gl = null;

var CANVAS;

var PREVIOUS_FRAME = 0;
var SPEED = 5;
var GRAVITY = 2;
var ROTATE_SPEED = 2;

var START_POSITION = {x: 4, y: 4, z: 4};
var PLAYERS_FOUND = 0;
var MAX_PLAYERS = 5;
var PLAYER_LOCATIONS = [];

var CAMERA = {
   proj: new Matrix4(),
   view: new Matrix4(),
   pos: {x: START_POSITION.x, y: START_POSITION.y, z: START_POSITION.z, pitch: 0, yaw: Math.PI * 0.75},
   acc: {x: 0, y: 0, z: 0}
};

{
   var rotate = Matrix4.Rotate(0, CAMERA.pos.yaw, 0);
   var translate = Matrix4.Translate(-CAMERA.pos.x, -CAMERA.pos.y, -CAMERA.pos.z);
   
   CAMERA.view = Matrix4.Multiply(translate, rotate);
}

var SUN_DIR = {x: 1, y: 4, z: 1};

var PLAYER_HEIGHT = 1;
var AIM_HEIGHT = CAMERA.pos.y;
var SMOOTH_Y_TRANSITION = false;

var TEXTURED_SHADER = new Shader();
var INSTANCED_SHADER = new Shader();
var INSTANCED_TEXTURED_SHADER = new Shader();
var FOOTPRINTS_SHADER = new Shader();

var MODELS = [];
var MODEL_TEXTURES = [];

CANVAS = document.getElementById("glcanvas");
CANVAS.oncontextmenu = function(e) {
   e.preventDefault();
};

if (!InitWebGL()) return;

gl.clearColor(0.6, 0.8, 0.9, 1.0);
gl.enable(gl.CULL_FACE);
gl.enable(gl.DEPTH_TEST);
gl.enable(gl.BLEND);
gl.blendFunc( gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
gl.pixelStorei(gl.UNPACK_PREMULTIPLY_ALPHA_WEBGL, false);
gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);

window.addEventListener("resize", ResizeCanvas);

document.onkeydown = InputManager.HandleKeyDown;
document.onkeyup = InputManager.HandleKeyUp;

InputManager.CustomiseControls(InputManager.Controls.MOVE_FORWARD, "W".charCodeAt(0));
InputManager.CustomiseControls(InputManager.Controls.MOVE_BACKWARD, "S".charCodeAt(0));
InputManager.CustomiseControls(InputManager.Controls.MOVE_LEFT, "A".charCodeAt(0));
InputManager.CustomiseControls(InputManager.Controls.MOVE_RIGHT, "D".charCodeAt(0));

InputManager.CustomiseControls(InputManager.Controls.ROTATE_LEFT, 37);
InputManager.CustomiseControls(InputManager.Controls.ROTATE_RIGHT, 39);

InputManager.CustomiseControls(InputManager.Controls.INTERACT, "E".charCodeAt(0));

ResizeCanvas();

TEXTURED_SHADER.Init(gl);
TEXTURED_SHADER.CreateShader(ShaderSources.GetShaderSource("drawVertex"), gl.VERTEX_SHADER);
TEXTURED_SHADER.CreateShader(ShaderSources.GetShaderSource("drawFragment"), gl.FRAGMENT_SHADER);
TEXTURED_SHADER.CreateProgram();

INSTANCED_SHADER.Init(gl);
INSTANCED_SHADER.CreateShader(ShaderSources.GetShaderSource("drawVertexInstanced"), gl.VERTEX_SHADER);
INSTANCED_SHADER.CreateShader(ShaderSources.GetShaderSource("drawFragmentInstanced"), gl.FRAGMENT_SHADER);
INSTANCED_SHADER.CreateProgram();

INSTANCED_TEXTURED_SHADER.Init(gl);
INSTANCED_TEXTURED_SHADER.CreateShader(ShaderSources.GetShaderSource("drawVertexInstanced"), gl.VERTEX_SHADER);
INSTANCED_TEXTURED_SHADER.CreateShader(ShaderSources.GetShaderSource("drawFragmentInstancedTextured"), gl.FRAGMENT_SHADER);
INSTANCED_TEXTURED_SHADER.CreateProgram();

FOOTPRINTS_SHADER.Init(gl);
FOOTPRINTS_SHADER.CreateShader(ShaderSources.GetShaderSource("drawVertexFootprint"), gl.VERTEX_SHADER);
FOOTPRINTS_SHADER.CreateShader(ShaderSources.GetShaderSource("drawFragmentFootprint"), gl.FRAGMENT_SHADER);
FOOTPRINTS_SHADER.CreateProgram();

var CUBE_SIZE = 0.25;
var TERRAIN = new InstancedModel();
TERRAIN.InitGL(gl);
TERRAIN.SetScale(CUBE_SIZE, CUBE_SIZE, CUBE_SIZE);

var MAP = [];
var MAP_OBJECTS = [];

{
   var positions = [];

   for (var x = 0; x < 256; x++) {
      MAP[x] = [];
      for (var z = 0; z < 256; z++) {
         var y = Math.round(Noise.GetNoise2D(x, z, 0.01) * 10);
   
         positions[positions.length] = x * CUBE_SIZE;
         positions[positions.length] = y * CUBE_SIZE;
         positions[positions.length] = z * CUBE_SIZE;
   
         MAP[x][z] = y * CUBE_SIZE;
      }
   }

   TERRAIN.SetPerInstanceData(positions, 3, 3);
}

var FOOTPRINT_TEXTURE = new Texture();
FOOTPRINT_TEXTURE.InitGL(gl);
FOOTPRINT_TEXTURE.LoadTexture("Textures/footprints.png");

var FOOTPRINTS = new InstancedModel();
FOOTPRINTS.InitGL(gl);
FOOTPRINTS.SetVertexData([
   -0.5, 0, 0.5,    0, 1,    0, 1, 0,
   0.5, 0, 0.5,     1, 1,    0, 1, 0,
   0.5, 0, -0.5,    1, 0,    0, 1, 0,

   0.5, 0, -0.5,    1, 0,    0, 1, 0,
   -0.5, 0, -0.5,   0, 0,    0, 1, 0,
   -0.5, 0, 0.5,    0, 1,    0, 1, 0,
]);
FOOTPRINTS.SetTextureID(FOOTPRINT_TEXTURE.GetID());
FOOTPRINTS.SetScale(CUBE_SIZE, CUBE_SIZE, CUBE_SIZE);

SetupGame();

// LoadAllModels();

// GenerateAllPaths();


function InitWebGL() {
   try {
      gl = CANVAS.getContext("webgl2", {alpha: true});
   }
   catch(e) {
   }

   //--------------------------------------------------------------------------
   // If we don't have a GL context, give up now

   if (!gl) {
      alert("Unable to initialize WebGL. Your browser may not support it.");
      return false;
   }

   return true;
}


function Update(currentFrame) {

   var dt = Math.min((currentFrame - PREVIOUS_FRAME) * 0.001, 0.032);
   PREVIOUS_FRAME = currentFrame;

   CAMERA.acc.y -= GRAVITY * dt;
   HandleControls(dt);
   Draw();

   window.requestAnimationFrame(Update);
}


function Draw() {

   //--------------------------------------------------------------------------
   // Clear the canvas before we start drawing on it.

   gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);


   //--------------------------------------------------------------------------
   // Draw all the instanced models that do not have textures.

   INSTANCED_SHADER.Use();
   INSTANCED_SHADER.UniformMatrix4fv("proj", CAMERA.proj);
   INSTANCED_SHADER.UniformMatrix4fv("view", CAMERA.view);

   INSTANCED_SHADER.Uniform3f("lightDir", SUN_DIR.x, SUN_DIR.y, SUN_DIR.z);
   INSTANCED_SHADER.Uniform3f("colour", 0.1, 0.5, 0.2);

   TERRAIN.Draw(INSTANCED_SHADER);


   //--------------------------------------------------------------------------
   // Draw all the instanced models that do have textures.

   INSTANCED_TEXTURED_SHADER.Use();
   INSTANCED_TEXTURED_SHADER.UniformMatrix4fv("proj", CAMERA.proj);
   INSTANCED_TEXTURED_SHADER.UniformMatrix4fv("view", CAMERA.view);

   INSTANCED_TEXTURED_SHADER.Uniform3f("lightDir", SUN_DIR.x, SUN_DIR.y, SUN_DIR.z);

   for (var i = 0; i < MODELS.length; i++) {
      MODELS[i].Draw(INSTANCED_TEXTURED_SHADER);
   }


   //--------------------------------------------------------------------------
   // Draw all the footprints.

   FOOTPRINTS_SHADER.Use();
   FOOTPRINTS_SHADER.UniformMatrix4fv("proj", CAMERA.proj);
   FOOTPRINTS_SHADER.UniformMatrix4fv("view", CAMERA.view);

   FOOTPRINTS_SHADER.Uniform3f("lightDir", SUN_DIR.x, SUN_DIR.y, SUN_DIR.z);

   FOOTPRINTS.Draw(FOOTPRINTS_SHADER);
}


function HandleControls(dt) {
   var move = {x: 0, y: 0, z: 0, pitch: 0, yaw: 0};
   var didMove = false;

   move.y += CAMERA.acc.y;
   didMove = true;

   if (InputManager.IsMovingForward()) {
      move.x -= SPEED * Math.cos(CAMERA.pos.yaw + Math.PI * 0.5);
      move.z -= SPEED * Math.sin(CAMERA.pos.yaw + Math.PI * 0.5);
      didMove = true;
   }

   if (InputManager.IsMovingBackward()) {
      move.x += SPEED * Math.cos(CAMERA.pos.yaw + Math.PI * 0.5);
      move.z += SPEED * Math.sin(CAMERA.pos.yaw + Math.PI * 0.5);
      didMove = true;
   }

   if (InputManager.IsMovingLeft()) {
      move.x -= SPEED * Math.cos(CAMERA.pos.yaw);
      move.z -= SPEED * Math.sin(CAMERA.pos.yaw);
      didMove = true;
   }

   if (InputManager.IsMovingRight()) {
      move.x += SPEED * Math.cos(CAMERA.pos.yaw);
      move.z += SPEED * Math.sin(CAMERA.pos.yaw);
      didMove = true;
   }

   if (InputManager.IsRotatingLeft()) {
      move.yaw -= ROTATE_SPEED;
      didMove = true;
   }


   if (InputManager.IsRotatingRight()) {
      move.yaw += ROTATE_SPEED;
      didMove = true;
   }

   
   if (didMove) {
      CAMERA.pos.x += move.x * dt;
      CAMERA.pos.y += move.y * dt;
      CAMERA.pos.z += move.z * dt;
      CAMERA.pos.yaw += move.yaw * dt;

      HandleCollision("X", move.x * dt);
      HandleCollision("Z", move.z * dt);
      HandleCollision("Y", move.y * dt);

      if (SMOOTH_Y_TRANSITION) {
         var diff = AIM_HEIGHT - CAMERA.pos.y;
         if (Math.abs(diff) < 0.01) {
            CAMERA.pos.y = AIM_HEIGHT;
            SMOOTH_Y_TRANSITION = false;
         } else {
            CAMERA.pos.y += diff * 0.1;
         }
      }

      var rotate = Matrix4.Rotate(0, CAMERA.pos.yaw, 0);
      var translate = Matrix4.Translate(-CAMERA.pos.x, -CAMERA.pos.y, -CAMERA.pos.z);
      
      CAMERA.view = Matrix4.Multiply(translate, rotate);
   }


   if (InputManager.Interact()) {
      var minDistanceSq = (6 * CUBE_SIZE) * (6 * CUBE_SIZE);

      for (var i = PLAYER_LOCATIONS.length - 1; i >= 0; i--) {
         var vec = new Vector3(
            PLAYER_LOCATIONS[i].x - CAMERA.pos.x, 
            0, 
            PLAYER_LOCATIONS[i].z - CAMERA.pos.z);

         var distanceSq = Vector3.Dot(vec, vec);

         if (distanceSq < minDistanceSq) {
            PLAYERS_FOUND++;
            PLAYER_LOCATIONS.splice(i, 1);

            document.getElementById("count-text").innerText = PLAYERS_FOUND + "/" + MAX_PLAYERS;

            if (PLAYERS_FOUND == MAX_PLAYERS) {
               document.getElementById("win-text").style.display = "block";
            }
         }
      }
   }
}


function HandleCollision(axis, movedBy) {
   var xIndex = Clamp(Math.floor(CAMERA.pos.x / CUBE_SIZE), 0, MAP.length - 1);
   var zIndex = Clamp(Math.floor(CAMERA.pos.z / CUBE_SIZE), 0, MAP.length - 1);

   if (CAMERA.pos.x < 0 || CAMERA.pos.z < 0 || 
       CAMERA.pos.x > MAP.length * CUBE_SIZE || CAMERA.pos.z > MAP.length * CUBE_SIZE) {
      
      if (axis == "X") CAMERA.pos.x -= movedBy;
      if (axis == "Z") CAMERA.pos.z -= movedBy;
   }

   if (MAP[xIndex][zIndex] > CAMERA.pos.y * CUBE_SIZE - PLAYER_HEIGHT) {
      AIM_HEIGHT = MAP[xIndex][zIndex] + PLAYER_HEIGHT;
      SMOOTH_Y_TRANSITION = true;
      CAMERA.acc.y = 0;
   }
}


async function SetupGame() {
   await LoadAllModels();

   GenerateAllPaths();
}


function GenerateAllPaths() {
   var numPaths = MAX_PLAYERS;
   var stepSize = 1;

   var positions = [];
   var colours = [];

   for(var i = 0; i < numPaths; i++) {
      var randomIndex = Math.floor(Math.random() * MAP_OBJECTS.length);
      var endPosition = MAP_OBJECTS[randomIndex];

      PLAYER_LOCATIONS[PLAYER_LOCATIONS.length] = {x: endPosition.x, y: endPosition.y, z: endPosition.z};

      GeneratePath(positions, colours, START_POSITION, endPosition, stepSize);
   }

   FOOTPRINTS.SetPerInstanceData(positions, 4, 3);
   FOOTPRINTS.SetPerInstanceData(colours, 1, 4);
}

function GeneratePath(positions, colours, startPosition, endPosition, stepSize) {
   var currentPosition = {
      x: startPosition.x,
      z: startPosition.z 
   };

   var distance = (endPosition.x - currentPosition.x) * (endPosition.x - currentPosition.x) +
                  (endPosition.z - currentPosition.z) * (endPosition.z - currentPosition.z);

   var originalDistance = distance;

   var maxDistance = CUBE_SIZE * CUBE_SIZE;

   while (distance > maxDistance) {

      //--------------------------------------------------------------------------
      // Find the vector straight to the end position.

      var alignVector = new Vector3(
         endPosition.x - currentPosition.x,
         0,
         endPosition.z - currentPosition.z);

      alignVector = Vector3.GetNormalised(alignVector);


      //--------------------------------------------------------------------------
      // Find a random vector within a 45 deg region of the align vector.

      var randomAngle = Math.random() * Math.PI * 0.5 - Math.PI * 0.25;

      var cos = Math.cos(randomAngle);
      var sin = Math.sin(randomAngle);

      var randomVector = new Vector3(
         alignVector.x * cos + alignVector.z * sin,
         0,
         alignVector.z * cos - alignVector.x * sin);


      //--------------------------------------------------------------------------
      // Linearly interpolate between the two vectors weighting more on the align
      // vector the closer to the end point the current vector is.
      
      var newVector = Vector3.Lerp(alignVector, randomVector, distance / originalDistance);

      currentPosition.x += newVector.x * stepSize;
      currentPosition.y += newVector.y * stepSize;
      currentPosition.z += newVector.z * stepSize;


      //--------------------------------------------------------------------------
      // Find the closest block to the position and align the new position to it.

      var indexX = Clamp(Math.floor(currentPosition.x / CUBE_SIZE), 0, MAP.length - 1);
      var indexZ = Clamp(Math.floor(currentPosition.z / CUBE_SIZE), 0, MAP.length - 1);

      var x = indexX * CUBE_SIZE;
      var y = MAP[indexX][indexZ] + CUBE_SIZE * 0.5 + 0.001;
      var z = indexZ * CUBE_SIZE;
      var angle = 0;

      currentPosition.x = x;
      currentPosition.z = z;

      distance =  (endPosition.x - currentPosition.x) * (endPosition.x - currentPosition.x) +
                  (endPosition.z - currentPosition.z) * (endPosition.z - currentPosition.z);


      //--------------------------------------------------------------------------
      // Rotate the footprints to match the previous one with the current one.

      if (positions.length > 0) {
         var prevX = positions[positions.length - 4];
         var prevZ = positions[positions.length - 2];

         var vec1 = new Vector3(x - prevX, 0, z - prevZ);
         vec1 = Vector3.GetNormalised(vec1);

         var vec2 = new Vector3(0, 0, 1);

         var prevAngle = Math.acos(Vector3.Dot(vec1, vec2));

         positions[positions.length - 1] = -prevAngle;
      }

      positions[positions.length] = x;
      positions[positions.length] = y;
      positions[positions.length] = z;
      positions[positions.length] = angle;

      colours[colours.length] = distance / originalDistance;
   }
}


async function LoadAllModels() {
   var res = await fetch("Models/model-list.txt");
   var text = await res.text();

   var lines = text.split('\n');

   for (var i = 0; i < lines.length; i++) {
      await LoadModel("Models/", lines[i]);

      var positions = [];

      var box = MODELS[i].GetBoundingBox();
   
      var centre = {x: 0, y: 0, z: 0};
      centre.x = (box.minX + box.maxX) * 0.5;
      centre.y = (box.minY + box.maxY) * 0.5;
      centre.z = (box.minZ + box.maxZ) * 0.5;
   
      for (var j = 0; j < 50; j++) {
         var x = Math.random() * MAP.length * CUBE_SIZE;
         var z = Math.random() * MAP.length * CUBE_SIZE;
   
         var xIndex = Clamp(Math.floor(x / CUBE_SIZE), 0, MAP.length - 1);
         var zIndex = Clamp(Math.floor(z / CUBE_SIZE), 0, MAP.length - 1);
   
         var y = MAP[xIndex][zIndex] + CUBE_SIZE * 0.5;
   
         positions[positions.length] = x - centre.x;
         positions[positions.length] = y - centre.y;
         positions[positions.length] = z - centre.z;

         MAP_OBJECTS[MAP_OBJECTS.length] = {
            x: x - centre.x,
            y: y - centre.y,
            z: z - centre.z};
      }
   
      MODELS[i].SetPerInstanceData(positions, 3, 3);

      if (lines[i].includes("tree")) {
         MODELS[i].SetScale(4, 4, 4);
      } else if (lines[i].includes("bush")) {
         MODELS[i].SetScale(0.5, 0.5, 0.5);
      }
   }

   window.requestAnimationFrame(Update);
}


async function LoadModel(dir, fileName) {
   var res = await fetch(dir + fileName);
   var text = await res.text();

   var lines = text.split('\n');

   var mtlFileName = "";

   var vertices = [];
   var normals = [];
   var texCoords = [];

   var vertexData = [];

   for (var i = 0; i < lines.length; i++) {
      var l = lines[i].split(' ');
      l[0] = l[0].trim();

      if (l[0] == "mtllib") {
         mtlFileName = l[1];

      } else if (l[0] == "vn") {
         normals[normals.length] = parseFloat(l[1]);
         normals[normals.length] = parseFloat(l[2]);
         normals[normals.length] = parseFloat(l[3]);

      } else if (l[0] == "vt") {
         texCoords[texCoords.length] = parseFloat(l[1]);
         texCoords[texCoords.length] = parseFloat(l[2]);

      } else if (l[0] == "v") {
         vertices[vertices.length] = parseFloat(l[1]);
         vertices[vertices.length] = parseFloat(l[2]);
         vertices[vertices.length] = parseFloat(l[3]);

      } else if (l[0] == "f") {
         for (var j = 1; j < 4; j++) {
            var v = l[j].split('/');

            var vertexIndex = parseInt(v[0]) - 1;
            var textureIndex = parseInt(v[1]) - 1;
            var normalIndex = parseInt(v[2]) - 1;

            vertexData[vertexData.length] = vertices[vertexIndex * 3 + 0];
            vertexData[vertexData.length] = vertices[vertexIndex * 3 + 1];
            vertexData[vertexData.length] = vertices[vertexIndex * 3 + 2];

            vertexData[vertexData.length] = texCoords[textureIndex * 2 + 0];
            vertexData[vertexData.length] = texCoords[textureIndex * 2 + 1];

            vertexData[vertexData.length] = normals[normalIndex * 3 + 0];
            vertexData[vertexData.length] = normals[normalIndex * 3 + 1];
            vertexData[vertexData.length] = normals[normalIndex * 3 + 2];
         }
      }
   }

   var modelIndex = MODELS.length;
   MODELS[modelIndex] = new InstancedModel();
   MODELS[modelIndex].InitGL(gl);
   MODELS[modelIndex].SetVertexData(vertexData);

   await LoadMTL(dir, mtlFileName);

   MODELS[modelIndex].SetTextureID(MODEL_TEXTURES[MODEL_TEXTURES.length - 1].GetID());
}


async function LoadMTL(dir, filePath) {
   var res = await fetch(dir + filePath);
   var text = await res.text();

   var lines = text.split('\n');

   var texFilePath = "";
   
   for (var i = 0; i < lines.length; i++) {
      var l = lines[i].split(' ');
      l[0] = l[0].trim();

      if (l[0] == "map_Kd") {
         texFilePath = l[1];
      }
   }

   var modelTextureIndex = MODEL_TEXTURES.length;
   MODEL_TEXTURES[modelTextureIndex] = new Texture();
   MODEL_TEXTURES[modelTextureIndex].InitGL(gl);
   await MODEL_TEXTURES[modelTextureIndex].LoadTexture(dir + texFilePath);
}


function ResizeCanvas() {
   CANVAS = document.getElementById("glcanvas");
   CANVAS.width = CANVAS.clientWidth;
   CANVAS.height = CANVAS.clientHeight;

   gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight);

   CAMERA.proj = Matrix4.GetPerspective(Math.PI * 0.25, CANVAS.width / CANVAS.height, 0.1, 10000);
}


function Clamp(t, min, max) {
   t = Math.max(t, min);
   t = Math.min(t, max);

   return t;
}


})();