var InputManager = (function() {

   var currentlyPressedKeys = {};
   var controllerInput = false;

   var controls = {
      MOVE_FORWARD: "MoveForward",
      MOVE_BACKWARD: "MoveBackward",
      MOVE_LEFT: "MoveLeft",
      MOVE_RIGHT: "MoveRight",
      ROTATE_LEFT: "RotateLeft",
      ROTATE_RIGHT: "RotateRight",
      INTERACT: "Interact"
   }

   var moveForwardKeyCode = "W".charCodeAt(0);
   var moveBackwardKeyCode = "S".charCodeAt(0);
   var moveLeftKeyCode = "A".charCodeAt(0);
   var moveRightKeyCode = "D".charCodeAt(0);

   var rotateLeftKeyCode = "O".charCodeAt(0);
   var rotateRightKeyCode = "P".charCodeAt(0);

   var interactKeyCode = "E".charCodeAt(0);


   function IsMovingForward() {
      return currentlyPressedKeys[moveForwardKeyCode] || controllerInput;
   }


   function IsMovingBackward() {
      return currentlyPressedKeys[moveBackwardKeyCode] || controllerInput;
   }


   function IsMovingLeft() {
      return currentlyPressedKeys[moveLeftKeyCode] || controllerInput;
   }


   function IsMovingRight() {
      return currentlyPressedKeys[moveRightKeyCode] || controllerInput;
   }


   function IsRotatingLeft() {
      return currentlyPressedKeys[rotateLeftKeyCode] || controllerInput;
   }


   function IsRotatingRight() {
      return currentlyPressedKeys[rotateRightKeyCode] || controllerInput;
   }


   function Interact() {
      return currentlyPressedKeys[interactKeyCode] || controllerInput;
   }


   function CustomiseControls(control, keyCode) {
      if (control == controls.MOVE_FORWARD) moveForwardKeyCode = keyCode;
      if (control == controls.MOVE_BACKWARD) moveBackwardKeyCode = keyCode;
      if (control == controls.MOVE_LEFT) moveLeftKeyCode = keyCode;
      if (control == controls.MOVE_RIGHT) moveRightKeyCode = keyCode;

      if (control == controls.ROTATE_LEFT) rotateLeftKeyCode = keyCode;
      if (control == controls.ROTATE_RIGHT) rotateRightKeyCode = keyCode;

      if (control == controls.INTERACT) interactKeyCode = keyCode;
   }


   function HandleKeyDown(event) {
      currentlyPressedKeys[event.keyCode] = true;
   }


   function HandleKeyUp(event) {
      currentlyPressedKeys[event.keyCode] = false;
   }


   return {
      IsMovingForward: IsMovingForward,
      IsMovingBackward: IsMovingBackward,
      IsMovingLeft: IsMovingLeft,
      IsMovingRight: IsMovingRight,
      IsRotatingLeft: IsRotatingLeft,
      IsRotatingRight: IsRotatingRight,
      Interact: Interact,

      CustomiseControls: CustomiseControls,
      Controls: controls,

      HandleKeyDown: HandleKeyDown,
      HandleKeyUp: HandleKeyUp
   }
})();