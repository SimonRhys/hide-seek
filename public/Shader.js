var Shader = function() {

   var gl;
   var program;
   var vertexShader;
   var fragmentShader;

   var uniforms = [];

   function Init(glContext) {
      gl = glContext;
   }

   function CreateShader(source, shaderType) {
      if(shaderType == gl.VERTEX_SHADER) {
         vertexShader = CompileShader(source, shaderType);
      } else if(shaderType == gl.FRAGMENT_SHADER) {
         fragmentShader = CompileShader(source, shaderType);        
      }
   }

   function CreateProgram() {
      program = gl.createProgram();
      gl.attachShader(program, vertexShader);
      gl.attachShader(program, fragmentShader);
      gl.linkProgram(program);

      if (!gl.getProgramParameter(program, gl.LINK_STATUS)) {
         console.log(gl.getProgramInfoLog(program));
      }
   }

   function GetAttribLocation(name) {
      return gl.getAttribLocation(program, name);
   }

   function Use() {
      gl.useProgram(program);
   }

   function Uniform1i(name, value) {
      CheckUniforms(name);
      gl.uniform1i(uniforms[name], value);
   }

   function Uniform1f(name, value) {
      CheckUniforms(name);
      gl.uniform1f(uniforms[name], value);

   }

   function Uniform2f(name, value1, value2) {
      CheckUniforms(name);
      gl.uniform2f(uniforms[name], value1, value2);
   }

   function Uniform3f(name, value1, value2, value3) {
      CheckUniforms(name);
      gl.uniform3f(uniforms[name], value1, value2, value3);
   }

   function Uniform4f(name, value1, value2, value3, value4) {
      CheckUniforms(name);
      gl.uniform4f(uniforms[name], value1, value2, value3, value4);
   }

   function UniformMatrix4fv(name, value) {
      CheckUniforms(name);
      gl.uniformMatrix4fv(uniforms[name], false, value.ToFloat32Array());
   }

   function CompileShader(source, shaderType) {
      var shader = gl.createShader(shaderType);
      gl.shaderSource(shader, source);
      gl.compileShader(shader);

      if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
         console.log(gl.getShaderInfoLog(shader));
         return null;
      }

      return shader;
   }

   function CheckUniforms(name) {
      if(uniforms[name] == null) {
         uniforms[name] = gl.getUniformLocation(program, name);
      }
   }

   return {
      Init: Init,
      CreateShader: CreateShader,
      CreateProgram: CreateProgram,
      GetAttribLocation: GetAttribLocation,
      Use: Use,
      Uniform1i: Uniform1i,
      Uniform1f: Uniform1f,
      Uniform2f: Uniform2f,
      Uniform3f: Uniform3f,
      Uniform4f: Uniform4f,
      UniformMatrix4fv: UniformMatrix4fv,
   }
};