var Noise = (function() {

   function GetNoise2D(x, y, frequency) {
      return Math.abs(Math.sin(x * frequency * 1.35264245) + Math.cos(y * frequency * 6.3323421789)) * 0.5;
   }

   return {
      GetNoise2D: GetNoise2D,
   }
})();