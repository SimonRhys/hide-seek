var ShaderSources = (function() {

   var SOURCES = {};

   SOURCES.drawVertex = `#version 300 es
      precision mediump float;

      layout(location=0) in vec3 position;
      layout(location=1) in vec2 texCoords;
      layout(location=2) in vec3 normal;

      uniform mat4 proj;
      uniform mat4 view;
      uniform mat4 model;

      out vec3 vPos;
      out vec2 vTexCoords;
      out vec3 vNormal;

      void main(void) 
      {
         vec4 mPos = model * vec4(position, 1.0);
         gl_Position = proj * view * mPos;  

         vPos = mPos.xyz;
         vTexCoords = texCoords;
         vNormal = mat3(transpose(inverse(model))) * normal;
      }
   `;

   SOURCES.drawFragment = `#version 300 es
      precision mediump float;

      in vec3 vPos;
      in vec2 vTexCoords;
      in vec3 vNormal;

      uniform sampler2D tex;

      out vec4 fragColour;
      void main(void) 
      {  
         vec3 lightPos = vec3(20.0, 10.0, 2.0);

         vec3 normal = normalize(vNormal);
         vec3 lightDir = normalize(lightPos - vPos);

         float ambient = 0.1;
         float diffuse = max(dot(normal, lightDir), 0.0);

         vec3 colour = texture(tex, vTexCoords).xyz;

         fragColour = vec4(colour * (ambient + diffuse), 1.0);
      }
   `;


   SOURCES.drawVertexInstanced = `#version 300 es
      precision mediump float;

      layout(location=0) in vec3 position;
      layout(location=1) in vec2 texCoords;
      layout(location=2) in vec3 normal;
      layout(location=3) in vec3 offset;

      uniform mat4 proj;
      uniform mat4 view;
      uniform mat4 model;

      out vec3 vPos;
      out vec2 vTexCoords;
      out vec3 vNormal;

      void main(void) 
      {
         vec4 mPos = model * vec4(position, 1.0);
         mPos.xyz += offset;
         
         gl_Position = proj * view * mPos;  

         vPos = mPos.xyz;
         vTexCoords = texCoords;
         vNormal = mat3(transpose(inverse(model))) * normal;
      }
   `;


   SOURCES.drawFragmentInstanced = `#version 300 es
      precision mediump float;

      in vec3 vPos;
      in vec3 vNormal;

      uniform vec3 lightDir;
      uniform vec3 colour;

      out vec4 fragColour;
      void main(void) 
      {  
         vec3 normal = normalize(vNormal);
         vec3 normLightDir = normalize(lightDir);

         float ambient = 0.15;
         float diffuse = max(dot(normal, normLightDir), 0.0);

         fragColour = vec4(colour * (ambient + diffuse), 1.0);
      }
   `;


   SOURCES.drawFragmentInstancedTextured = `#version 300 es
      precision mediump float;

      in vec3 vPos;
      in vec2 vTexCoords;
      in vec3 vNormal;

      uniform sampler2D tex;
      uniform vec3 lightDir;

      out vec4 fragColour;
      void main(void) 
      {  
         vec3 normal = normalize(vNormal);
         vec3 normLightDir = normalize(lightDir);

         float ambient = 0.15;
         float diffuse = max(dot(normal, normLightDir), 0.0);

         vec4 colour = texture(tex, vTexCoords);
         colour.xyz = colour.xyz * (ambient + diffuse);

         fragColour = colour;
      }
   `;


   SOURCES.drawVertexFootprint = `#version 300 es
      precision mediump float;

      layout(location=0) in vec3 position;
      layout(location=1) in vec2 texCoords;
      layout(location=2) in vec3 normal;
      layout(location=3) in vec4 offset;
      layout(location=4) in float colourFade;

      uniform mat4 proj;
      uniform mat4 view;
      uniform mat4 model;

      out vec3 vPos;
      out vec2 vTexCoords;
      out vec3 vNormal;
      out float vColourFade;

      void main(void) 
      {
         mat4 rotMatrix = mat4(
            cos(offset.w), 0.0, sin(offset.w), 0.0,
            0.0, 1.0, 0.0, 0.0,
            -sin(offset.w), 0.0, cos(offset.w), 0.0,
            0.0, 0.0, 0.0, 1.0
         );

         vec4 mPos = rotMatrix * vec4(position, 1.0);
         mPos = model * mPos;
         mPos.xyz += offset.xyz;
         
         gl_Position = proj * view * mPos;  

         vPos = mPos.xyz;
         vTexCoords = texCoords;
         vNormal = mat3(transpose(inverse(model))) * normal;
         vColourFade = colourFade;
      }
   `;


   SOURCES.drawFragmentFootprint = `#version 300 es
      precision mediump float;

      in vec3 vPos;
      in vec2 vTexCoords;
      in vec3 vNormal;
      in float vColourFade;

      uniform sampler2D tex;
      uniform vec3 lightDir;

      out vec4 fragColour;
      void main(void) 
      {  
         vec3 normal = normalize(vNormal);
         vec3 normLightDir = normalize(lightDir);

         float ambient = 0.15;
         float diffuse = max(dot(normal, normLightDir), 0.0);

         vec3 warmColour = vec3(0.7411764705882353, 0.09411764705882353, 0.08627450980392157);
         vec3 coldColour = vec3(0.073, 0.202, 0.835);

         vec3 footprintColour = mix(warmColour, coldColour, vColourFade);

         vec4 colour = texture(tex, vTexCoords) * vec4(footprintColour, 1.0);
         colour.xyz = colour.xyz * (ambient + diffuse);

         fragColour = colour;
      }
   `;

   function GetShaderSource(name) {
         return SOURCES[name];
   }

   return {
      GetShaderSource: GetShaderSource,
   };

})();