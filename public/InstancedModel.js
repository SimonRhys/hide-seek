var InstancedModel = function() {

   var gl;

   var vao;
   var vbo;
   var numTriangles = 0;

   var positions = [];
   var scale = {x: 1, y: 1, z: 1};
   var boundingBox = {minX: 0, maxX: 0, minY: 0, maxY: 0, minZ: 0, maxZ: 0};
   var modelMatrix = new Matrix4();
   var textureID;
   var hasTexture = false;

   var numModels = 0;


   function InitGL(webgl) {
      gl = webgl;

      vao = gl.createVertexArray();
      vbo = gl.createBuffer();

      var verts = [
         // Front Face
         -0.5, -0.5, 0.5,     0, 0,       0, 0, 1,
         0.5, -0.5, 0.5,      0, 0,       0, 0, 1,
         0.5, 0.5, 0.5,       0, 0,       0, 0, 1,

         0.5, 0.5, 0.5,       0, 0,       0, 0, 1,
         -0.5, 0.5, 0.5,      0, 0,       0, 0, 1,
         -0.5, -0.5, 0.5,     0, 0,       0, 0, 1,

         // Right Face
         0.5, -0.5, 0.5,      0, 0,       1, 0, 0,
         0.5, -0.5, -0.5,     0, 0,       1, 0, 0,
         0.5, 0.5, -0.5,      0, 0,       1, 0, 0,

         0.5, 0.5, -0.5,      0, 0,       1, 0, 0,
         0.5, 0.5, 0.5,       0, 0,       1, 0, 0,
         0.5, -0.5, 0.5,      0, 0,       1, 0, 0,

         // Back Face
         0.5, -0.5, -0.5,     0, 0,       0, 0, -1,
         -0.5, -0.5, -0.5,    0, 0,       0, 0, -1,
         -0.5, 0.5, -0.5,     0, 0,       0, 0, -1,

         -0.5, 0.5, -0.5,     0, 0,       0, 0, -1,
         0.5, 0.5, -0.5,      0, 0,       0, 0, -1,
         0.5, -0.5, -0.5,     0, 0,       0, 0, -1,

         // Left Face
         -0.5, -0.5, -0.5,    0, 0,       -1, 0, 0,
         -0.5, -0.5, 0.5,     0, 0,       -1, 0, 0,
         -0.5, 0.5, 0.5,      0, 0,       -1, 0, 0,

         -0.5, 0.5, 0.5,      0, 0,       -1, 0, 0,
         -0.5, 0.5, -0.5,     0, 0,       -1, 0, 0,
         -0.5, -0.5, -0.5,    0, 0,       -1, 0, 0,

         // Top Face
         -0.5, 0.5, 0.5,      0, 0,       0, 1, 0,
         0.5, 0.5, 0.5,       0, 0,       0, 1, 0,
         0.5, 0.5, -0.5,      0, 0,       0, 1, 0,

         0.5, 0.5, -0.5,      0, 0,       0, 1, 0,
         -0.5, 0.5, -0.5,     0, 0,       0, 1, 0,
         -0.5, 0.5, 0.5,      0, 0,       0, 1, 0,

         // Bot Face
         -0.5, -0.5, -0.5,    0, 0,       0, -1, 0,
         0.5, -0.5, -0.5,     0, 0,       0, -1, 0,
         0.5, -0.5, 0.5,      0, 0,       0, -1, 0,

         0.5, -0.5, 0.5,      0, 0,       0, -1, 0,
         -0.5, -0.5, 0.5,     0, 0,       0, -1, 0,
         -0.5, -0.5, -0.5,    0, 0,       0, -1, 0
      ];

      SetVertexData(verts);
   }

   
   function Draw(shader) {
      gl.bindVertexArray(vao);

      if (hasTexture) {
         gl.activeTexture(gl.TEXTURE0);
         gl.bindTexture(gl.TEXTURE_2D, textureID);
         shader.Uniform1i("tex", 0);
      }

      shader.UniformMatrix4fv("model", modelMatrix);

      gl.drawArraysInstanced(gl.TRIANGLES, 0, numTriangles, numModels);

      gl.bindVertexArray(null);
   }


   function SetPerInstanceData(data, numElementsPerInstance, vertexShaderLocation) {
      numModels = data.length / numElementsPerInstance;

      gl.bindVertexArray(vao);

      var instanceVBO = gl.createBuffer();
      gl.bindBuffer(gl.ARRAY_BUFFER, instanceVBO);
      gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(data), gl.DYNAMIC_DRAW);
      gl.vertexAttribPointer(vertexShaderLocation, numElementsPerInstance, gl.FLOAT, false, 0, 0);
      gl.vertexAttribDivisor(vertexShaderLocation, 1);
      gl.enableVertexAttribArray(vertexShaderLocation);

      gl.bindVertexArray(null);
   }


   function GetBoundingBox() {
      return boundingBox;
   }


   function SetScale(x, y, z) {
      scale.x = x;
      scale.y = y;
      scale.z = z;

      modelMatrix = Matrix4.Scale(x, y, z);
   }


   function SetTextureID(id) {
      textureID = id;
      hasTexture = true;
   }
 

   function SetVertexData(vertexData) {
      for (var i = 0; i < vertexData.length; i+=8) {
         boundingBox.minX = Math.min(vertexData[i + 0], boundingBox.minX);
         boundingBox.minY = Math.min(vertexData[i + 1], boundingBox.minX);
         boundingBox.minZ = Math.min(vertexData[i + 2], boundingBox.minX);

         boundingBox.maxX = Math.max(vertexData[i + 0], boundingBox.maxX);
         boundingBox.maxY = Math.max(vertexData[i + 1], boundingBox.maxX);
         boundingBox.maxZ = Math.max(vertexData[i + 2], boundingBox.maxX);
      }

      numTriangles = Math.floor(vertexData.length / 8);

      gl.bindVertexArray(vao);
      
      gl.bindBuffer(gl.ARRAY_BUFFER, vbo);
      gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertexData), gl.STATIC_DRAW);
      gl.vertexAttribPointer(0, 3, gl.FLOAT, false, 32, 0);
      gl.enableVertexAttribArray(0);
      gl.vertexAttribPointer(1, 2, gl.FLOAT, false, 32, 12);
      gl.enableVertexAttribArray(1);
      gl.vertexAttribPointer(2, 3, gl.FLOAT, false, 32, 20);
      gl.enableVertexAttribArray(2);

      gl.bindVertexArray(null);
   }


   return {
      InitGL: InitGL,
      Draw: Draw,
      GetBoundingBox: GetBoundingBox,
      SetPerInstanceData: SetPerInstanceData,
      SetScale: SetScale,
      SetTextureID: SetTextureID,
      SetVertexData: SetVertexData,
   }
 };