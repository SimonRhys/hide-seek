# Hide and Seek #

This repository contains my January entry into the One Game A Month challenge with a theme of "Hide and Seek".

### Game Description ###

This is a simple game where you must follow the footprints to find your friends that are hiding from you. A 'warmer' colour implies a more recent footprint and a colder colour implies an older footprint.

The game can be found at the following link: 

### Controls ###

W - Forward
S - Backward
A - Strafe Left
D - Strafe Right

Left Arrow - Rotate Left
Right Arrow - Rotate Right